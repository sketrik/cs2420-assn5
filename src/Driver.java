import java.io.File;
import java.util.Scanner;

public class Driver {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter word to autocomplete (or multiple comma-separated words; i.e. word1, word2, word3): ");
		String[] words = scanner.nextLine().toLowerCase().split(", ");
		System.out.println("Enter number of results to see: ");
		int limit = scanner.nextInt();
		Term[] terms = readFile("SortedWords.txt");
		if (terms == null) {
			System.out.println("Invalid file.");
			return;
		}
		LeftistHeap<Term> heap = new LeftistHeap<>();
		for (String word : words) {
			int begin = firstIndexOf(terms, word);
			int end = lastIndexOf(terms, word);
			LeftistHeap<Term> tempHeap = new LeftistHeap<>();
			while (begin <= end) {
				tempHeap.insert(terms[begin++]);
			}
			heap.merge(tempHeap);
		}
		while (limit > 0) {
			Term term = heap.deleteMax();
			if (term == null) {
				System.out.println("No more words.");
				break;
			}
			System.out.println(term.word);
			limit--;
		}

	}

	private static int firstIndexOf(Term a[], String target) {
		int begin = 0;
		int end = a.length - 1;
		int mid = 0;
		while (begin <= end) {
			mid = (begin + end) / 2;
			String word = a[mid].word;
			if (target.compareTo(word) > 0) {
				begin = mid + 1;
			} else {
				end = mid - 1;
			}
		}
		return mid;
	}

	private static int lastIndexOf(Term a[], String target) {
		target += "{";
		int begin = 0;
		int end = a.length - 1;
		int mid = 0;
		while (begin <= end) {
			mid = (begin + end) / 2;
			String word = a[mid].word;
			if (target.compareTo(word) > 0) {
				begin = mid + 1;
			} else {
				end = mid - 1;
			}
		}
		return mid - 1;
	}


//	private int lastIndexOf(Term a[], String target, int r) {
//
//	}

	public static Term[] readFile(String fileName) {
		try {
			Scanner reader = new Scanner(new File(fileName));
			Term[] array = new Term[reader.nextInt()];
			int i = 0;
			while ((reader.hasNext())) {
				String word = reader.next();
				long freq = reader.nextInt();
				Term term = new Term(word, freq);
//				System.out.println(term);
				array[i++] = term;
			}
			reader.close();
			return array;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
