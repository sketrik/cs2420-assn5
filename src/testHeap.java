import java.util.ArrayList;
import java.util.Collections;

import org.junit.Assert;
import org.junit.Test;

public class testHeap {

	public static void main(String[] args) {
		LeftistHeap<Integer> heap = new LeftistHeap<>();
		ArrayList<Integer> list = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			list.add(i);
		}
		Collections.shuffle(list);

		for (Integer item : list) {
			heap.insert(item);
		}
		heap.printHeap("Heap1");

		Collections.shuffle(list);

		LeftistHeap<Integer> heap2 = new LeftistHeap<>();
		for (Integer item : list) {
			heap2.insert(item);
		}
		heap2.merge(heap);
		heap2.printHeap("Heap2");
		heap2.checkLeftist();
		heap2.checkHeap();
		heap2.deleteMax();
		heap2.printHeap("Heap2 (deleted max)");
		heap2.checkLeftist();
		heap2.checkHeap();
	}
}
