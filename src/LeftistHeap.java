public class LeftistHeap<E extends Comparable<? super E>> {
	private Node<E> root;

	LeftistHeap() {

	}

	public E deleteMax() {
		if (root == null) {
			return null;
		}
		E value = root.value;
		this.root = merge(root.right, root.left);
		return value;
	}

	public void insert(E value) {
		Node<E> node = new Node<>(value);
		this.root = merge(this.root, node);
	}

	public void merge(LeftistHeap<E> t2) {
		this.root = merge(root, t2.root);
	}

	private Node<E> merge(Node<E> n1, Node<E> n2) {
		if (n1 == null) {
			return n2;
		} else if (n2 == null) {
			return n1;
		}
		Node<E> newRoot;
		if (n1.value.compareTo(n2.value) >= 0) {
			newRoot = n1;
			newRoot.right = merge(newRoot.right, n2);
		} else {
			newRoot = n2;
			newRoot.right = merge(newRoot.right, n1);
		}
		getNPL(newRoot);
		trySwitchKids(newRoot);
		return newRoot;
	}

	private int getNPL(Node<E> node) {
		if (node == null) {
			return -1;
		}
		return Math.min(getNPL(node.right), getNPL(node.left)) + 1;
	}


	private void trySwitchKids(Node<E> n) {
		if (n.right == null) {
			return;
		}
		if (n.left == null || n.right.npl > n.left.npl) {
			Node<E> temp = n.left;
			n.left = n.right;
			n.right = temp;
		}
	}

	public void printHeap(String label) {
		System.out.println("--------" + label + "---------");
		printHeap(root, "");
	}

	private void printHeap(Node<E> node, String indent) {
		if (node != null) {
			printHeap(node.right, indent + "   ");
			System.out.println(indent + node.value);
			printHeap(node.left, indent + "   ");
		}
	}

	public void checkLeftist() {
		if (!checkLeftist(this.root)) {
			System.out.println("Ouch. Not a leftist heap.");
		} else {
			System.out.println("We good! This is a leftist heap.");
		}
	}

	private boolean checkLeftist(Node<E> node) {
		if (node.left == null) {
			return node.right == null;
		}
		if (node.right == null) {
			return true;
		}
		return node.left.npl >= node.right.npl && checkLeftist(node.left) && checkLeftist(node.right);
	}

	public void checkHeap() {
		if (!checkHeap(this.root)) {
			System.out.println("Ouch. Not a heap.");
		} else {
			System.out.println("We good! This is heap.");
		}
	}

	private boolean checkHeap(Node<E> node) {
		if (node == null) {
			return true;
		}
		boolean test = true;
		if (node.right != null) {
			test = test && node.value.compareTo(node.right.value) >= 0;
		}
		if (node.left != null) {
			test = test && node.value.compareTo(node.left.value) >= 0;
		}
		return test;
	}

	private class Node<E> {
		E value;
		Node<E> left;
		Node<E> right;
		int npl = 0;

		Node(E value) {
			this.value = value;
		}
	}
}
